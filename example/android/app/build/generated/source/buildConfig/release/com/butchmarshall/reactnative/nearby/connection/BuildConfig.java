/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.butchmarshall.reactnative.nearby.connection;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.butchmarshall.reactnative.nearby.connection";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 2;
  public static final String VERSION_NAME = "1.1";
}
